/* Copyright 2020 Lary Gibaud
 *
 * This file is part of LudusSonis/lsnodes.
 *
 * lsnodes is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * lsnodes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with lsnodes.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef SCENE_H
#define SCENE_H

#include <sys/queue.h>

#include "node.h"

typedef struct scene {
  char* name;
  Node* rootnode;
  jack_client_t* jack_client;
} Scene;

void attachRootNode(Scene*, Node*);
struct slistnodehead gatherPdNodes(Scene*);
void launchScene(Scene*);
void connectScene(Scene*);

#endif
