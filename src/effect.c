/* Copyright 2020 Lary Gibaud
 *
 * This file is part of LudusSonis/lsnodes.
 *
 * lsnodes is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * lsnodes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with lsnodes.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "effect.h"

void
init_effect(Effect* this, char* pd_filename)
{
  init_pdnode((PdNode*)this, 1, 1, pd_filename);
}

Effect*
new_effect(char* pd_filename)
{
  Effect* this = malloc(sizeof(PdNode));
  if (!this)
    error(EXIT_FAILURE, 0, "malloc");

  init_effect(this, pd_filename);

  return this;
}

void
destroy_effect(Effect* this)
{
  destroy_pdnode((PdNode*)this);
}
