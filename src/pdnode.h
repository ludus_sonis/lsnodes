/* Copyright 2020 Lary Gibaud
 *
 * This file is part of LudusSonis/lsnodes.
 *
 * lsnodes is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * lsnodes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with lsnodes.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef PDNODE_H
#define PDNODE_H

#include <sys/types.h>
#include <unistd.h>
#include <stdbool.h>

#include <jack/jack.h>

#include "node.h"

typedef enum {
  INERT,
  REGISTERING,
  REGISTERED,
  READY_FOR_CONNECT,
  CONNECTED
} PdNodeState;

#define PDNODE_CLASS NODE_CLASS \
  pthread_mutex_t state_mtx; \
  pthread_cond_t state_cond; \
  PdNodeState state; \
  unsigned n_inports; \
  unsigned n_outports; \
  char* pd_filename; \
  char* canoname; \
  pid_t pid; \
  int fildes[2]; \
  jack_client_t* jack_client; \
  jack_port_t* inports[2]; \
  jack_port_t* outports[2];

typedef struct pdnode {
  PDNODE_CLASS
} PdNode;

void init_pdnode(PdNode*, int n_inports, int n_outports,
    char* pd_filename);
PdNode* new_pdnode(int n_inports, int n_outports, char* pd_filename);
void destroy_pdnode(PdNode*);
void pdNodeSetClientRegister(PdNode*, bool);
void pdNodeSetPortRegister(PdNode*, jack_port_id_t, bool);

SLIST_HEAD(slistpdnodehead, pdnode);

#endif
