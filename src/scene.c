/* Copyright 2020 Lary Gibaud
 *
 * This file is part of LudusSonis/lsnodes.
 *
 * lsnodes is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * lsnodes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with lsnodes.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "scene.h"

void
attachRootNode(Scene* scene, Node* node)
{
  scene->rootnode = node;
  node->scene = scene;
  node->setCanoname(node);
}

struct slistnodehead
gatherPdNodes(Scene* scene)
{
  struct slistnodehead head = SLIST_HEAD_INITIALIZER(head);
  nodeGatherPdNodes(scene->rootnode, &head);
  return head;
}

void
launchScene(Scene* scene)
{
  nodeRecLaunch(scene->rootnode);
}

void
connectScene(Scene* scene)
{
  scene->rootnode->connect(scene->rootnode, NULL);
}
