/* Copyright 2020 Lary Gibaud
 *
 * This file is part of LudusSonis/lsnodes.
 *
 * lsnodes is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * lsnodes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with lsnodes.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <time.h>

#include <assert.h>

#include "pdnode.h"
#include "scene.h"

static PdNode*
asPdNode(Node* this)
{
  return (PdNode*)this;
}

static void
setCanoname(Node* this)
{
  PdNode* pdthis = asPdNode(this);
  const char* scenename = this->scene->name;
  size_t snamelen = strlen(scenename);
  size_t canonamelen = snamelen + 1 + strlen(pdthis->pd_filename) + 1;
  if (canonamelen > jack_client_name_size())
    error(EXIT_FAILURE, 0, "nodeSetCanoname: canonical name size too long");
  pdthis->canoname = malloc(canonamelen);
  if (!pdthis->canoname)
    error(EXIT_FAILURE, errno, "nodeSetCanoname: malloc");
  strcpy(pdthis->canoname, scenename);
  pdthis->canoname[snamelen] = '*';
  pdthis->canoname[snamelen+1] = 0;
  strcat(pdthis->canoname, pdthis->pd_filename);
}

static void
pdNodeLock(PdNode* this)
{
  int ret = pthread_mutex_lock(&this->state_mtx);
  if (ret)
    error(EXIT_FAILURE, ret, "pdNodeLock");
}

static void
pdNodeUnlock(PdNode* this)
{
  int ret = pthread_mutex_unlock(&this->state_mtx);
  if (ret)
    error(EXIT_FAILURE, ret, "pdNodeUnlock");
}

static void
pdNodeWaitCond(PdNode* this)
{
  struct timespec ts;
  if (clock_gettime(CLOCK_REALTIME_COARSE, &ts))
    error(EXIT_FAILURE, errno, "clock_gettime");
  ts.tv_sec += 10;
  int ret = pthread_cond_timedwait(&this->state_cond, &this->state_mtx, &ts);
  if (ret)
    error(EXIT_FAILURE, ret, "pthread_cond_timedwait");
}

static void
pdNodeSignal(PdNode* this)
{
  int ret = pthread_cond_signal(&this->state_cond);
  if (ret)
    error(EXIT_FAILURE, ret, "pdNodeSignal");
}

static void
connect(Node* _this, Node* that)
{
  PdNode* this = (PdNode*)_this;
  pdNodeLock(this);
  if (READY_FOR_CONNECT != this->state)
    pdNodeWaitCond(this);
  //TODO: remove
  assert(this->state == READY_FOR_CONNECT);
  for (unsigned u=0; u<this->n_outports; ++u) {
    const char* thatport;
    if (that)
      thatport = jack_port_name(((PdNode*)that)->inports[u]);
    else
      thatport = (u ? "playback_2" : "playback_1");
    int ret = jack_connect(this->scene->jack_client, jack_port_name(this->outports[u]), thatport);
    if (ret)
      error(EXIT_FAILURE, 0, "jack_connect");
  }
  Node* child;
  SLIST_FOREACH(child, &_this->children, entries)
    child->connect(child, _this);
}

static void
launch(Node* _this)
{
  PdNode* this = (PdNode*)_this;
  //I probably don't need to lock here
  //if I am not dumb
  this->state = REGISTERING;
  if(pipe(this->fildes))
      error(EXIT_FAILURE, errno, "pipe");
  this->pid = fork();
  if (this->pid < 0)
    error(EXIT_FAILURE, errno, "fork");
  if (0 == this->pid) {
    if (close(this->fildes[0]))
      error(EXIT_FAILURE, errno, "close");
    if (-1 == dup2(this->fildes[1], 1))
      error(EXIT_FAILURE, errno, "dup2");
    char inchans[8], outchans[8];
    int ret = snprintf(inchans, sizeof(inchans)-1, "%d", this->n_inports);
    if (ret < 0 || ret >= sizeof(inchans))
      error(EXIT_FAILURE, 0, "snprintf");
    ret = snprintf(outchans, sizeof(outchans)-1, "%d", this->n_outports);
    if (ret < 0 || ret >= sizeof(outchans))
      error(EXIT_FAILURE, 0, "snprintf");
    ret = execlp("pd", "-nogui", "-jack", "-jackname", this->canoname,
        "-nojackconnect", "-inchannels", inchans, "-outchannels", outchans,
        this->pd_filename, NULL);
    if (-1 == ret)
      error(EXIT_FAILURE, errno, "execlp");
  } //else in parent
  if (close(this->fildes[1]))
    error(EXIT_FAILURE, errno, "close");
}

static void
checkNPorts(unsigned nports)
{
  if (nports > 2)
    error(EXIT_FAILURE, 0, "checkNPorts: can't have more than 2 ports (input or output)");
}

void
init_pdnode(PdNode* this, int n_inports, int n_outports,
    char* pd_filename)
{
  checkNPorts(n_inports);
  checkNPorts(n_outports);

  init_node((Node*)this);

  this->launch = launch;
  this->asPdNode = asPdNode;
  this->setCanoname = setCanoname;
  this->connect = connect;

  pthread_mutex_init(&this->state_mtx, NULL);
  pthread_cond_init(&this->state_cond, NULL);
  this->state = INERT;
  this->n_inports = n_inports;
  this->n_outports = n_outports;
  this->pd_filename = pd_filename;
}

PdNode*
new_pdnode(int n_inports, int n_outports, char* pd_filename)
{
  PdNode* this = malloc(sizeof(PdNode));
  if (!this)
    error(EXIT_FAILURE, 0, "malloc");

  init_pdnode(this, n_inports, n_outports, pd_filename);

  return this;
}

void
destroy_pdnode(PdNode* this)
{
  destroy_node((Node*) this);
}

void
pdNodeSetClientRegister(PdNode* this, bool reg)
{
  if (reg) {
    if (REGISTERING != this->state)
      error(EXIT_FAILURE, 0, "pdNodeSetClientRegister: spurious register");
    return;
  }
  error(EXIT_FAILURE, 0, "pdNodeSetClientRegister: spurious unregister");
}

static unsigned
nRegPorts(PdNode* this, jack_port_t** ap)
{
  return (ap[0]?1:0) + (ap[1]?1:0);
}

void
pdNodeSetPortRegister(PdNode* this, jack_port_id_t port, bool reg)
{
  if (REGISTERED != this->state)
    error(EXIT_FAILURE, 0, "pdNodeSetPortRegister: spurious register, client not registered");
  jack_port_t* jack_port = jack_port_by_id(this->scene->jack_client, port);
  int flags = jack_port_flags(jack_port);
  int nports;
  jack_port_t** ports;
  unsigned u;
  if (flags & JackPortIsInput) {
    ports = this->inports;
    nports = this->n_inports;
  } else {
    ports = this->outports;
    nports = this->n_outports;
  }
  if (reg) {
    for (u=0; u<nports; ++u) {
      if (ports[u] == jack_port)
        error(EXIT_FAILURE, 0, "pdNodeSetPortRegister: port already registered");
    }
    if (nRegPorts(this, ports) >= nports)
      error(EXIT_FAILURE, 0, "pdNodeSetPortRegister: no space for more port");
    for(u=0; u<nports; ++u) {
      if (NULL == ports[u]) {
        ports[u] = jack_port;
        break;
      }
    }
    if (nRegPorts(this, ports) == nports) { //ready for connect
      pdNodeLock(this);
      this->state = READY_FOR_CONNECT;
      pdNodeSignal(this);
      pdNodeUnlock(this);
    }
  } else
    error(EXIT_FAILURE, 0, "pdNodeSetPortRegister: spurious unregister");
}
