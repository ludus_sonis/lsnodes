/* Copyright 2020 Lary Gibaud
 *
 * This file is part of LudusSonis/lsnodes.
 *
 * lsnodes is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * lsnodes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with lsnodes.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "jackevent.h"

jack_client_t* g_jackclient;

pthread_mutex_t g_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t g_cond = PTHREAD_COND_INITIALIZER;

TAILQ_HEAD(jackeventtailq, jackevent) g_head = TAILQ_HEAD_INITIALIZER; 

int g_eventmask = 0;
int g_nportscon = 0;
int g_nportsreg = 0;

static void
lock_check(void)
{
  int ret = pthread_mutex_lock(&g_mutex);
  if (0 != ret)
    error(EXIT_FAILURE, ret, "pthread_mutex_lock");
}

static void
unlock_check(void)
{
  int ret = pthread_mutex_unlock(&g_mutex);
  if (0 != ret)
    error(EXIT_FAILURE, ret, "pthread_mutex_unlock");
}

static void
sendEvent(JackEvent* ev)
{
  lock_check();
  if (g_eventmask & ev->type) {
    TAILQ_INSERT_HEAD(&g_head, ev, entries);
    int ret = pthread_cond_signal(&g_cond);
    if (0 != ret)
      error(EXIT_FAILURE, ret, "pthread_cond_signal");
  }
  unlock_check();
}

/*
 * Proper use is:
 * maskEvents()
 * //stuff that launches client, port, setup connections
 * getEvent() while the mask is not zero to empty the queue + 1
 * Actually, you should now exactly how many events you are
 * waiting for. For example client register + 2 ports register
 * 4 calls to getEvent()
 * The last one will return NULL and is only to empty the queue,
 * no new events will be placed in it because mask is 0.
 * Queue emptied you can make new requests.
 *
 * may be better to return a list of events? that way less
 * lock() and unlock() and maybe less spurious events because
 * we keep the lock to treat them all.
 * Maybe later.
 */

void
maskEvents(bool client, int nportsreg, int nportscon)
{
  lock_check();
  if (client)
    g_eventmask |= CLIENTREGISTER;
  if (nportsreg) {
    g_eventmask |= PORTREGISTER;
    g_nportsreg = nportsreg;
  }
  if (nportscon) {
    g_eventmask |= PORTSCONNECT;
    g_nportscon = nportscon;
  }
  unlock_check();
}


JackEvent*
getEvent(int)
{
  JackEvent* ev = NULL;
  lock_check();
  while(g_mask) {
    ev = TAILQ_LAST(&g_head, jackeventtailq);
    if (NULL == ev) { //no event yet
      int ret = pthread_cond_wait(&g_cond, &g_mutex);
      if (0 != ret)
        error(EXIT_FAILURE, ret, "pthread_cond_wait");
    } else if (g_eventmask & ev->type) { //interesting
      if (CLIENTREGISTER == ev->type)
        g_eventmask &= ~CLIENTREGISTER;
      else if (PORTREGISTER == ev->type) {
        --g_nportsreg;
        if (!g_nportsreg)
          g_eventmask &= ~PORTREGISTER;
      } else {
        --g_nportscon;
        if (!g_nportscon)
          g_eventmask &= ~PORTSCON;
      }
      TAILQ_REMOVE(&g_head, ev, entries);
      unlock_check();
      return ev;
    } else { //not interesting
      TAILQ_REMOVE(&g_head, ev, entries);
      free(ev);
    }
  } //now empty the queue
  while (!TAILQ_EMPTY(&g_head)) {
    ev = TAILQ_LAST(&g_head, jackeventtailq);
    TAILQ_REMOVE(&head, ev, entries);
    free(ev);
  }
  unlock_check();
  return NULL;
}

static void
jack_shutdown_cb(void* arg)
{
  error(EXIT_FAILURE, 0, "jack server down");
  //other stuff probably needed
}

static void
jack_client_register_cb(const char* name, int registered, void* arg)
{
  JackEvent* ev = malloc(sizeof(JackEvent));
  if (NULL == ev)
    error(EXIT_FAILURE, 0, "malloc");
  ev->type = CLIENTREGISTER;
  ev->element.clientreg.name = strdup(name);
  if (NULL == ev->element.clientreg.name)
    error(EXIT_FAILURE, errno, "strdup");
  ev->element.clientreg.reg = registered;

  sendEvent(ev);
}

static void
jack_port_register_cb(jack_port_id_t port, int registered, void* arg)
{
  JackEvent* ev = malloc(sizeof(JackEvent));
  if (NULL == ev)
    error(EXIT_FAILURE, 0, "malloc");
  ev->type = PORTREGISTER;
  ev->element.portreg.port = port;
  ev->element.portreg.reg = registered;

  sendEvent(ev);
}

static void
jack_port_connect_cb(jack_port_id_t a, jack_port_id_t b, int connect, void* arg)
{
  JackEvent* ev = malloc(sizeof(JackEvent));
  if (NULL == ev)
    error(EXIT_FAILURE, 0, "malloc");
  ev->type = PORTSCONNECT;
  ev->element.portscon.a = a;
  ev->element.portscon.b = b;
  ev->element.portscon.connect = connect;

  sendEvent(ev);
}

void
initJackEvents(void)
{
  jack_options_t options = JackNullOption;
  jack_status_t status;

  g_jackclient = jack_client_open("Hyppocampus", options, &status);
  if (NULL == g_jackclient)
    error(EXIT_FAILURE, 0, "jack_client_open failed with status 0x%2.0x\n", status);

  if (status & JackServerStarted)
    printf("Jack server started\n");

  if (status & JackNameNotUnique)
    error(EXIT_FAILURE, 0, "client name already in use");

  jack_on_shutdown(client, jack_shutdown_cb, 0);

  jack_set_client_registration_callback(client, jack_client_register_cb, 0);

  jack_set_port_registration_callback(client, jack_port_register_cb, 0);

  jack_set_port_connect_callback(client, jack_port_connect_cb, 0);

  jack_activate(client);
}
