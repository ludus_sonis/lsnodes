/* Copyright 2020 Lary Gibaud
 *
 * This file is part of LudusSonis/lsnodes.
 *
 * lsnodes is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * lsnodes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with lsnodes.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef NODE_H
#define NODE_H

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <error.h>
#include <errno.h>
#include <sys/queue.h>
#include <jack/jack.h>

SLIST_HEAD(slistnodehead, node);

struct scene;
struct pdnode;
#define NODE_CLASS \
  struct scene* scene; \
  SLIST_ENTRY(node) entries; \
  struct node* parent; \
  struct slistnodehead children; \
  void (*process)(struct node*, float);\
  void (*launch)(struct node*); \
  struct pdnode* (*asPdNode)(struct node*); \
  void (*setCanoname)(struct node*); \
  void (*connect)(struct node*, struct node*);


typedef struct node {
  NODE_CLASS
} Node;

void init_node(Node* );
void destroy_node(Node*);
void addChild(Node*, Node*);
void nodeGatherPdNodes(Node*, struct slistnodehead*);
void nodeRecLaunch(Node*);
void nodeConnect(Node*, Node*);

#endif
