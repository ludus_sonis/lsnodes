/* Copyright 2020 Lary Gibaud
 *
 * This file is part of LudusSonis/lsnodes.
 *
 * lsnodes is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * lsnodes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with lsnodes.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "sound.h"

void
init_sound(Sound* this, char* pd_filename)
{
  init_pdnode((PdNode*)this, 0, 1, pd_filename);
}

Sound*
new_sound(char* pd_filename)
{
  Sound* this = malloc(sizeof(Sound));
  if (!this)
    error(EXIT_FAILURE, 0, "malloc");

  init_sound(this, pd_filename);

  return this;
}

void
destroy_sound(Sound* this)
{
  destroy_pdnode((PdNode*)this);
}

