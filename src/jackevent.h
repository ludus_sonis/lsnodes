/* Copyright 2020 Lary Gibaud
 *
 * This file is part of LudusSonis/lsnodes.
 *
 * lsnodes is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * lsnodes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with lsnodes.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef JACK_H
#define JACK_H

#include <pthread.h>
#include <sys/queue.h>
#include <stdio.h>
#include <error.h>
#include <stdlib.h>
#include <errno.h>
#include <signal.h>

#include <jack/jack.h>

typedef enum {
  CLIENTREGISTER = 1,
  PORTREGISTER = 2,
  PORTSCONNECT = 4
} JackEventType;

typedef struct {
  char* name;
  int reg;
} ClientRegisterEvent;

typedef struct {
  jack_port_id_t port;
  int reg;
} PortRegisterEvent;

typedef struct {
  jack_port_id_t a, b;
  int connect;
} PortsConnectEvent;

typedef union {
  ClientRegisterEvent clientreg;
  PortRegisterEvent portreg;
  PortsConnectsEvent portscon;
} JackEventElement;

typedef struct jackevent {
  JackEventType type;
  JackEventElement element;
  TAILQ_ENTRY(jackevent) entries;
} JackEvent;

void initJackEvents();
void maskEvents(bool client, int nportsreg, int nportscon);
JackEvent* getEvent(void);


#endif
