/* Copyright 2020 Lary Gibaud
 *
 * This file is part of LudusSonis/lsnodes.
 *
 * lsnodes is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * lsnodes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with lsnodes.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "node.h"
#include "pdnode.h"

static void
process(Node* this, float dt){}

static void
launch(Node* this){}

static PdNode*
asPdNode(Node* this)
{
  return NULL;
}

static void
setCanoname(Node* this) {}

/*
 * that is the closest parent node with ports
 * If null no such node between rootnode
 * and this, connect to system
 */
void
connect(Node* this, Node* that)
{
  Node* child;
  SLIST_FOREACH(child, &this->children, entries)
    connect(child, that);
}

void
init_node(Node* this)
{
  this->scene = NULL;
  this->parent = NULL;
  SLIST_INIT(&this->children);
  this->process = process;
  this->launch = launch;
  this->asPdNode = asPdNode;
  this->setCanoname = setCanoname;
}

void destroy_node(Node* this) {}

/*
 * TODO: rename to nodeAddChild and add addChild method,
 * we need to check connections
 * ie: can't addChild(Sound, Effect), for example
 */
void
addChild(Node* this, Node* child)
{
  if (!this->scene)
    error(EXIT_FAILURE, 0, "addChild: this must belong to a scene");
  child->scene = this->scene;

  SLIST_INSERT_HEAD(&this->children, child, entries);
  child->parent = this;
  child->setCanoname(child);
}

void
nodeGatherPdNodes(Node* this, struct slistnodehead* head)
{
  PdNode* pdthis = this->asPdNode(this);
  if (pdthis)
    SLIST_INSERT_HEAD(head, this, entries);
  Node* child;
  SLIST_FOREACH(child, &this->children, entries)
    nodeGatherPdNodes(child, head);
}

void
nodeRecLaunch(Node* this)
{
  this->launch(this);
  Node* child;
  SLIST_FOREACH(child, &this->children, entries)
    nodeRecLaunch(this);
}


