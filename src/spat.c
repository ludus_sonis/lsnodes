/* Copyright 2020 Lary Gibaud
 *
 * This file is part of LudusSonis/lsnodes.
 *
 * lsnodes is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * lsnodes is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with lsnodes.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "spat.h"

void
init_spat(Spat* this)
{
  init_pdnode((PdNode*)this, 1, 2, "spat.pd");
}

Spat*
new_spat(void)
{
  Spat* this = malloc(sizeof(PdNode));
  if (!this)
    error(EXIT_FAILURE, 0, "malloc");

  init_spat(this);

  return this;
}

void
destroy_spat(Spat* this)
{
  destroy_pdnode((PdNode*)this);
}
